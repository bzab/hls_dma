#include <cstdlib>
#include <string.h>
#include <ap_int.h>
#include <ap_axi_sdata.h>
#include <hls_stream.h>
typedef ap_uint<64> AXI_VALUE;
typedef ap_axiu<64,0,0,0> AXIS_DATA;

#define BUFLEN (1024*1024*2/8)
const int TMPBUFLEN=256;

void dma1 (hls::stream<AXIS_DATA> &din, volatile AXI_VALUE *a){
#pragma HLS INTERFACE axis port=din
#pragma HLS INTERFACE m_axi depth=TMPBUFLEN port=a offset=direct

#pragma HLS DATAFLOW
  int i=0;
  AXI_VALUE buff[TMPBUFLEN];
  AXIS_DATA td;
  //memcpy creates a burst access to memory
  //multiple calls of memcpy cannot be pipelined and will be scheduled sequentially
  //memcpy requires a local buffer to store the results of the memory transaction
while(1) {
  i=0;
  while(1){
    #pragma HLS PIPELINE
    din.read(td);
    *(a+i) = td.data;
    if(td.last)
        break;
    else
    	i++;
  }
}
}
